import React, { ReactNode, useState } from "react";
import { Modal, message, Radio, Input } from "antd";
import { UserInfoTypes } from "../../types/login_type";
import AuthorityBtn from "../../components/authorityBtn";
interface Props {
  container: ReactNode;
  onClick?: () => void;
  onSuccess?: () => void;
  loginInfo?: UserInfoTypes;
}
const Report: React.FC<Props> = ({
  loginInfo,
  container,
  onSuccess,
  onClick,
}) => {
  const [reason, setReason] = useState("0");
  const options = [
    { label: "广告垃圾", value: "0" },
    { label: "违规内容", value: "1" },
    { label: "恶意灌水", value: "2" },
    { label: "重复发帖", value: "3" },
    { label: "其他", value: "4" },
  ];
  const [visible, setVisible] = useState(false);
  const handleOnClose = () => {
    setVisible(false);
    setReason("0");
  };
  return (
    <div>
      <AuthorityBtn
        onClick={() => {
          setVisible(true);
          onClick && onClick();
        }}
      >
        <div style={{ cursor: "pointer" }}>{container}</div>
      </AuthorityBtn>
      <Modal
        destroyOnClose
        visible={visible}
        title={"举报"}
        okText={"确定"}
        cancelText={"取消"}
        onOk={() => {
          message.success("举报成功，请等待后续处理", 1, () => {
            handleOnClose();
            onSuccess && onSuccess();
          });
        }}
        onCancel={() => {
          handleOnClose();
        }}
      >
        <h2>请选择举报理由</h2>
        <Radio.Group
          options={options}
          onChange={(e) => {
            setReason(e.target.value);
          }}
          value={reason}
        />
        {reason === "4" && (
          <Input.TextArea rows={4} maxLength={200} showCount />
        )}
      </Modal>
    </div>
  );
};
export default Report;
