import React, { ReactNode, FC } from "react";
import { connect } from "react-redux";
import { message } from "antd";
import { UserInfoTypes } from "../../types/login_type";
import { useRouter } from "next/router";

type AuthorityBtnTypes = {
  loginInfo: UserInfoTypes;
  onClick: () => void;
  children: ReactNode;
};

const AuthorityBtn: FC<AuthorityBtnTypes> = ({
  onClick,
  loginInfo,
  children,
}) => {
  const router = useRouter();
  const handleClick = () => {
    if (!loginInfo) {
      message.warning("请先登录", 1, () => {
        router.push(`/login?redirect=${window.location.href}`);
      });
    } else {
      onClick();
    }
  };
  return <div onClick={handleClick}>{children}</div>;
};
export default connect((state) => {
  console.log("权限按钮");
  console.log(state);
  return {
    loginInfo: state.userInfo.loginInfo,
  };
})(AuthorityBtn);
