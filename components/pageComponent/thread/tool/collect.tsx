import React, { useState, useEffect } from "react";
import Image from "next/image";
import styled from "styled-components";
import { UserInfoTypes } from "../../../../types/login_type";
import AuthorityBtn from "../../../../components/authorityBtn";
type Props = {
  loginInfo?: UserInfoTypes;
  isCollect: boolean;
  collect_num: number;
};
const CollectWrapper = styled.div`
  display: flex;
  position: relative;
  .like_num {
    color: #ffc34d;
    position: absolute;
    right: -10px;
    top: 7px;
    font-size: 12px;
  }
`;
const Collect: React.FC<Props> = ({ loginInfo, isCollect, collect_num }) => {
  const [userCollect, setUserCollect] = useState(false);
  useEffect(() => {
    setUserCollect(isCollect);
  }, [isCollect]);

  const toggleCollect = () => {
    // TODO 收藏逻辑
    setUserCollect((prev) => !prev);
  };
  return (
    <AuthorityBtn onClick={toggleCollect}>
      <CollectWrapper>
        {userCollect ? (
          <>
            <Image src="/image/collect.png" width={32} height={32} />
            <sup className="like_num">{collect_num}</sup>
          </>
        ) : (
          <>
            <Image src="/image/uncollect.png" width={32} height={32} />
            <sup className="like_num">{collect_num}</sup>
          </>
        )}
      </CollectWrapper>
    </AuthorityBtn>
  );
};
export default Collect;
