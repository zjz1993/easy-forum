import React from "react";
import styles from "../../../../pages/thread/styles.module.less";
import Image from "next/image";
import { UserInfoTypes } from "../../../../types/login_type";
import Like from "./like";
import Collect from "./collect";
type Props = {
  loginInfo?: UserInfoTypes;
  isLike: boolean;
  like_num: number;
  isCollect: boolean;
  collect_num: number;
};
const Tool: React.FC<Props> = ({
  loginInfo,
  isLike,
  like_num,
  isCollect,
  collect_num,
}) => {
  return (
    <div className={styles.tool_wrapper}>
      <div className={styles.tool_item}>
        <Like isLike={isLike} like_num={like_num} loginInfo={loginInfo} />
      </div>
      <div className={styles.tool_item}>
        <Collect
          isCollect={isCollect}
          collect_num={collect_num}
          loginInfo={loginInfo}
        />
      </div>
    </div>
  );
};
export default Tool;
