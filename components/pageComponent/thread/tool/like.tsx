import React, { useState, useEffect } from "react";
import Image from "next/image";
import styled from "styled-components";
import { UserInfoTypes } from "../../../../types/login_type";
import AuthorityBtn from "../../../../components/authorityBtn/index";
type Props = {
  loginInfo?: UserInfoTypes;
  isLike: boolean;
  like_num: number;
  mode?: "page" | "inline";
};
const LikeWrapper = styled.div`
  display: flex;
  position: relative;
  .like_num {
    color: #0082fc;
    position: absolute;
    right: -10px;
    top: 7px;
    font-size: 12px;
  }
  .like_num_small {
    color: #0082fc;
    font-size: 12px;
  }
`;
const Like: React.FC<Props> = ({
  mode = "page",
  loginInfo,
  isLike,
  like_num,
}) => {
  const [userLike, setUserLike] = useState(false);
  useEffect(() => {
    setUserLike(isLike);
  }, [isLike]);

  const toggleLike = () => {
    // TODO 点赞逻辑
    setUserLike((prev) => !prev);
  };
  return (
    <AuthorityBtn onClick={toggleLike}>
      <LikeWrapper>
        {userLike ? (
          <>
            {mode === "page" ? (
              <>
                <Image src="/image/like.svg" width={32} height={32} />
                <sup className="like_num">{like_num}</sup>
              </>
            ) : (
              <>
                <Image src="/image/like_small.svg" width={10} height={10} />
                <span className="like_num_small">{like_num}</span>
              </>
            )}
          </>
        ) : (
          <>
            {mode === "page" ? (
              <>
                <Image src="/image/unlike.svg" width={32} height={32} />
                <sup className="like_num">{like_num}</sup>
              </>
            ) : (
              <>
                <Image src="/image/unlike_small.svg" width={10} height={10} />
                <span className="like_num_small">{like_num}</span>
              </>
            )}
          </>
        )}
      </LikeWrapper>
    </AuthorityBtn>
  );
};
export default Like;
