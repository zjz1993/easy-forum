import React, { useState } from "react";
import styles from "../../../../pages/thread/styles.module.less";
import Image from "next/image";
import ToPersonPage from "../../../toPerson/toPerson";
import moment from "moment";
import Report from "../../../report";
import CommentEditor from "../../../commentEditor";
import { ReplyInfoTypes } from "../../../../types/thread_type";
import Like from "../tool/like";
import { UserInfoTypes } from "../../../../types/login_type";
type Props = {
  data: ReplyInfoTypes;
  loginInfo?: UserInfoTypes;
};
const Reply: React.FC<Props> = ({
  data = {
    isLike: false,
    like_num: 20,
    id: 1,
    isAuthor: false,
    author: "",
    authorid: 0,
    replyed_author: "",
    replyed_authorid: 0,
    createtime: 0,
    message: "",
  },
  loginInfo,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  return (
    <div className={styles.reply}>
      <div className={styles.reply_title}>
        <div className={styles.reply_title_info}>
          <div className={styles.reply_person}>
            <div className={styles.reply_person_avatar}>
              <Image src="/image/avatar.png" width="100%" height="100%" />
            </div>
            <ToPersonPage
              uid={data.authorid}
              className={styles.reply_person_name}
              name={data.author}
            />
            {/*<div className={styles.reply_person_name}>回复者</div>*/}
          </div>
          <div className={styles.divide}>回复</div>
          <span className={styles.replyed_person}>被回复者</span>
        </div>
        <div className={styles.replytime}>
          {moment(data.createtime).format("YYYY-MM-DD HH:mm:ss")}
        </div>
      </div>
      <div className={styles.reply_content}>
        <div className={styles.reply_html}>回复1</div>
      </div>
      <div className={styles.reply_tool_wrapper}>
        <div className={styles.reply_tool_wrapper_left}>
          <div className={styles.reply_tool_wrapper_left}>
            <Like
              isLike={data.isLike}
              like_num={data.like_num}
              mode="inline"
              loginInfo={loginInfo}
            />
            <div
              onClick={() => {
                setIsEditing(true);
              }}
            >
              评论
            </div>
          </div>
          {data.isAuthor && (
            <div className={styles.reply_tool_wrapper_left}>
              <div>编辑</div>
              <div>删除</div>
            </div>
          )}
        </div>
        <Report container={<div>举报</div>} loginInfo={loginInfo} />
      </div>
      {isEditing ? (
        <CommentEditor
          placeholder={"回复给这个人"}
          onBlur={() => {
            setIsEditing(false);
          }}
        />
      ) : null}
    </div>
  );
};
export default Reply;
