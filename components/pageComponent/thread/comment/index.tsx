import React, { useState } from "react";
import styles from "../../../../pages/thread/styles.module.less";
import Image from "next/image";
import ToPersonPage from "../../../toPerson/toPerson";
import moment from "moment";
import { Pagination, Popconfirm } from "antd";
import Report from "../../../report";
import Reply from "../reply";
import { CommentInfoTypes } from "../../../../types/thread_type";
import { UserInfoTypes } from "../../../../types/login_type";
import Like from "../tool/like";

interface Props {
  comment: CommentInfoTypes;
  loginInfo?: UserInfoTypes;
}

const Comment: React.FC<Props> = ({ comment, loginInfo }) => {
  const [commentData] = useState<CommentInfoTypes>(comment);
  return (
    <div className={styles.comment}>
      <div className={styles.title}>
        <div className={styles.title_info}>
          <div className={styles.title_info_avatar}>
            <Image src="/avatar.png" width="100%" height="100%" />
          </div>
          <div>
            <ToPersonPage
              uid={commentData.authorid}
              name={commentData.author}
              style={{ marginBottom: "5px" }}
            />
            <p className={styles.title_info_createtime}>
              发表于
              {moment(commentData.createtime).format("YYYY-MM-DD HH:mm:ss")}
            </p>
          </div>
        </div>
        {loginInfo
          ? commentData.isAuthor && (
              <div className={styles.manage}>
                <span>编辑</span>
                <Popconfirm
                  title="确定删除这条回复吗"
                  onConfirm={() => {}}
                  onCancel={() => {}}
                  okText="Yes"
                  cancelText="No"
                >
                  <span>删除</span>
                </Popconfirm>
              </div>
            )
          : null}
      </div>
      <div className={styles.detail}>
        <div className={styles.detailHtml}>1234</div>
      </div>
      <div className={styles.comment_tool_wrapper}>
        <div className={styles.comment_tool_wrapper_left}>
          <Like
            mode="inline"
            isLike={comment.isLike}
            like_num={comment.like_num}
            loginInfo={loginInfo}
          />
          <div>评论</div>
        </div>
        <Report
          container={
            <div className={styles.comment_tool_wrapper_right}>举报</div>
          }
        />
      </div>
      <div className={styles.reply_wrapper}>
        {commentData.reply.map((reply) => (
          <Reply key={reply.id} data={reply} loginInfo={loginInfo} />
        ))}
        {commentData.replyCount > 5 && (
          <Pagination
            size="small"
            total={commentData.replyCount}
            pageSize={5}
          />
        )}
      </div>
    </div>
  );
};
export default Comment;
