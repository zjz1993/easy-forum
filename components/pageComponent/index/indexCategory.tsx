import React, { useState } from "react";
import styles from "./styles.module.less";
import { Tag } from "antd";
type Props = {
  fid: number;
};
const { CheckableTag } = Tag;
const categoryArr = [
  {
    id: 0,
    name: "全部",
  },
  {
    id: 1,
    name: "话题2",
  },
  {
    id: 3,
    name: "很长很长的话题",
  },
  {
    id: 4,
    name: "很长很长的话题房间卡设计费肯定撒娇",
  },
];
const IndexCategory: React.FC<Props> = ({ fid: number }) => {
  const [activeId, setActiveId] = useState(0);
  return (
    <>
      {categoryArr.length > 0 ? (
        <div className={styles.category_wrapper}>
          当前节点下所有话题
          <div className={styles.content}>
            {categoryArr.map((item) => {
              return (
                <div key={item.id} className={styles.content_item}>
                  <CheckableTag
                    checked={item.id === activeId}
                    onClick={() => {
                      setActiveId(item.id);
                    }}
                  >
                    {item.name}
                  </CheckableTag>
                </div>
              );
            })}
          </div>
        </div>
      ) : null}
    </>
  );
};
export default IndexCategory;
