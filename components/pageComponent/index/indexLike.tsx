import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Image from "next/image";
import { UserInfoTypes } from "../../../types/login_type";
import AuthorityBtn from "../../../components/authorityBtn";
type Props = {
  agree_num: number;
  loginInfo?: UserInfoTypes;
  isAgree: boolean;
};
const Like = styled.div`
  width: 40px;
  height: 40px;
  background-color: ${(props) =>
    props.active ? "#0082fc" : "rgba(0, 130, 252, 0.4)"};
  padding: 5px;
  .icon {
    display: flex;
    justify-content: center;
  }
  .content_number {
    font-size: 12px;
    color: white;
    text-align: center;
  }
`;
const IndexLike: React.FC<Props> = ({ agree_num, loginInfo, isAgree }) => {
  const [isActive, setActive] = useState(false);
  useEffect(() => {
    setActive(isAgree);
  }, [isAgree]);

  return (
    <AuthorityBtn
      onClick={() => {
        // TODO 点赞逻辑
        setActive((prev) => !prev);
      }}
    >
      <Like
        active={isActive}
        // onClick={(e) => {
        //   e.preventDefault();
        //   if (!loginInfo) {
        //     message.warning("请先登录", 1, () => {
        //       router.push("/login");
        //     });
        //   } else {
        //     // TODO 点赞逻辑
        //     setActive((prev) => !prev);
        //   }
        // }}
      >
        <div className="icon">
          <Image src="/image/up_unactive.png" width={16} height={16} />
        </div>
        <p className="content_number">{agree_num}</p>
      </Like>
    </AuthorityBtn>
  );
};
export default IndexLike;
