import React, { useState } from "react";
import styles from "./styles.module.less";
import styled from "styled-components";
import { useRouter } from "next/router";
import { Button } from "antd";
import { useTranslation } from "next-i18next";
import { UserInfoTypes } from "../../../types/login_type";
import AuthorityBtn from "../../../components/authorityBtn";
const HomeLeftHeaderItem = styled.div`
  background: #fff;
  cursor: pointer;
  color: ${(props) => (props.active ? "#0082fc" : "#617288")};
  text-align: center;
  padding: 0 16px;
  height: 30px;
  line-height: 30px;
  border-radius: 2px;
`;
type Props = {
  loginInfo?: UserInfoTypes;
};
const IndexHeader: React.FC<Props> = ({ loginInfo }) => {
  const router = useRouter();
  const { t } = useTranslation("common");
  const [activeId, setActiveId] = useState(0);
  const [topic] = useState([
    {
      fid: 0,
      name: "推荐",
    },
    {
      fid: 1,
      name: "番薯交流",
    },
    {
      fid: 2,
      name: "番薯交流1",
    },
    {
      fid: 3,
      name: "番薯交流2",
    },
    {
      fid: 4,
      name: "番薯交流积分卡积分卡剑荡四方",
    },
    {
      fid: 5,
      name: "番薯交流积分卡积分卡剑荡四方福建奥德康师傅金卡戴珊就发",
    },
  ]);
  return (
    <div className={styles.header}>
      <div className={styles.header_left}>
        {topic.map((item) => {
          return (
            <HomeLeftHeaderItem
              key={item.fid}
              active={item.fid === activeId}
              onClick={() => {
                setActiveId(item.fid);
              }}
            >
              {item.name}
            </HomeLeftHeaderItem>
          );
        })}
      </div>
      <div>
        <AuthorityBtn
          onClick={() => {
            router.push("/forum/create");
          }}
        >
          <Button type="primary">{t("thread.post")}</Button>
        </AuthorityBtn>
      </div>
    </div>
  );
};
export default IndexHeader;
