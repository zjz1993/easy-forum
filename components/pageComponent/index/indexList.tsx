import React from "react";
import styles from "../../../pages/indexPage.module.less";
import Image from "next/image";
import Link from "next/link";
import moment from "moment";
import { GetStaticProps, GetStaticPaths, GetServerSideProps } from "next";
import { ThreadIndexInfoTypes } from "../../../types/thread_type";
import { UserInfoTypes } from "../../../types/login_type";
import IndexListSetting from "./indexListSetting";
import IndexLike from "./indexLike";
type Props = {
  item: ThreadIndexInfoTypes;
  key: number;
  loginInfo?: UserInfoTypes;
};
const IndexListItem: React.FC<Props> = ({ item, key, loginInfo }) => {
  return (
    <Link href={`/thread/${item.id}`} key={key}>
      <div className={styles.item}>
        <div className={styles.item_left}>
          <IndexLike
            agree_num={item.agree_num}
            isAgree={item.isAgree}
            loginInfo={loginInfo}
          />
        </div>
        <div className={styles.item_right}>
          <div className={styles.header}>
            <Link href={`/user/${item.authorId}`}>
              <div className={styles.header_info}>
                <div className={styles.avatar}>
                  <Image src="/image/avatar.png" width="100%" height="100%" />
                </div>
                <p>{item.author}</p>
              </div>
            </Link>
            <div>
              发布于：
              {moment(item.createtime * 1000).format("YYYY-MM-DD HH:mm:ss")}
            </div>
          </div>
          <p className={styles.title}>{item.title}</p>
          <div className={styles.item_right_footer}>
            <div className={styles.item_right_footer_item}>
              <div className={styles.icon}>
                <Image src="/image/collect_small.png" width={16} height={16} />
              </div>
              <p>{item.collect_num}</p>
            </div>
            <div className={styles.item_right_footer_item}>
              <div className={styles.icon}>
                <Image src="/image/comment.png" width={16} height={16} />
              </div>
              <p>{item.comment_num}</p>
            </div>
            <div className={styles.item_right_footer_item}>
              <div className={styles.icon}>
                <Image src="/image/send.png" width={16} height={16} />
              </div>
              <p>分享</p>
            </div>
            <IndexListSetting loginInfo={loginInfo} authorid={item.authorId} />
          </div>
        </div>
      </div>
    </Link>
  );
};
export default IndexListItem;
