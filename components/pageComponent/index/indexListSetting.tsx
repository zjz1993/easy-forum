import React, { useState } from "react";
import styles from "../../../pages/indexPage.module.less";
import Image from "next/image";
import { UserInfoTypes } from "../../../types/login_type";
import { Popover, Popconfirm } from "antd";
import styled from "styled-components";
type Props = {
  loginInfo?: UserInfoTypes;
  authorid: number;
};
const OperationMenu = styled.div`
  display: flex;
  flex-wrap: wrap;
  .item {
    padding: 10px;
    flex: 1;
    cursor: pointer;
    &:hover {
      color: #0082fc;
    }
  }
`;
const IndexListSetting: React.FC<Props> = ({ loginInfo, authorid }) => {
  const [visible, setVisible] = useState(false);
  const showSetting = loginInfo
    ? loginInfo.isAdmin || loginInfo.uid === authorid
    : false;
  return (
    <>
      {showSetting ? (
        <Popover
          visible={visible}
          onVisibleChange={(menuvisible) => {
            setVisible(menuvisible);
          }}
          content={
            <OperationMenu>
              <div
                className="item"
                onClick={(e) => {
                  e.preventDefault();
                  // TODO 编辑帖子逻辑
                  setVisible(false);
                }}
              >
                编辑
              </div>
              <Popconfirm
                title="Are you sure to delete this task?"
                onConfirm={(e) => {
                  // TODO 删除帖子的逻辑
                  e.preventDefault();
                  alert("删除");
                  setVisible(false);
                }}
                onCancel={(e) => {
                  e.preventDefault();
                  alert("取消");
                  setVisible(false);
                }}
                okText="Yes"
                cancelText="No"
              >
                <div className="item">删除</div>
              </Popconfirm>
            </OperationMenu>
          }
          title="操作菜单"
          trigger="click"
          placement="right"
        >
          <div
            className={styles.item_right_footer_item}
            onClick={(e) => {
              e.preventDefault();
            }}
          >
            <div className={styles.icon}>
              <Image src="/image/setting.png" width={16} height={16} />
            </div>
            <p>管理</p>
          </div>
        </Popover>
      ) : null}
    </>
  );
};
export default IndexListSetting;
