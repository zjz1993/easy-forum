import React from "react";
import styled from "styled-components";

const SVGCom = styled.div`
  fill: ${(props) => props.color};
  cursor: pointer;
  &:hover {
    fill: ${(props) => props.active_color};
  }
`;

interface Props {
  onClick?: () => void;
  color?: string;
  active_color?: string;
  children: React.ReactChild;
}

export default function SVGComponent(props: Props) {
  const { active_color = "#0082fc", children, color = "#333", onClick } = props;
  return (
    <SVGCom
      active_color={active_color}
      color={color}
      onClick={() => {
        onClick && onClick();
      }}
    >
      {children}
    </SVGCom>
  );
}
