import React from "react";
import styled from "styled-components";
import { Button, Input } from "antd";
import { useRouter } from "next/router";
import LoginedNav from "./nav/Logined_nav";
import { connect } from "react-redux";
import { UserInfoTypes } from "../types/login_type";
import { mediaQueries } from "../const/index";

const { Search } = Input;

const Header = styled.div`
  height: 65px;
  background: #fff;
  box-shadow: 0 3px 5px rgb(0 0 0 / 3%);
  opacity: 1;
`;
const HeaderContainer = styled.div`
  line-height: 65px;
  margin: 0 auto;
  max-width: 1005px;
  ${mediaQueries("lg")`
    width: 95%
  `};
  height: 65px;
  justify-content: space-between;
  display: flex;
`;
const HeaderContainerLeft = styled.div`
  display: flex;
  align-items: center;
`;
const HeaderContainerRight = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  button:nth-of-type(2) {
    margin-left: 10px;
  }
`;
const Logo = styled.div`
  margin-right: 10px;
  cursor: pointer;
`;
type Props = {
  loginInfo?: UserInfoTypes;
};

const Nav: React.FC<Props> = ({ loginInfo }) => {
  const router = useRouter();
  return (
    <Header>
      <HeaderContainer>
        <HeaderContainerLeft>
          <Logo onClick={() => router.push("/")}>logo</Logo>
          <Search
            placeholder="搜索"
            allowClear
            enterButton="搜索"
            size="large"
          />
        </HeaderContainerLeft>
        <HeaderContainerRight>
          {loginInfo ? (
            <LoginedNav loginInfo={loginInfo} />
          ) : (
            <>
              <Button
                type="primary"
                onClick={() =>
                  router.push({
                    pathname: "/login",
                    query: { refer: window.location.href },
                  })
                }
              >
                登录
              </Button>
              <Button onClick={() => router.push("/register")}>注册</Button>
            </>
          )}
        </HeaderContainerRight>
      </HeaderContainer>
    </Header>
  );
};
const mapStateToProps = (state) => {
  return {
    loginInfo: state.userInfo.loginInfo,
  };
};
export default connect(mapStateToProps)(Nav);
