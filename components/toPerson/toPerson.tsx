import React from "react";
import styled from "styled-components";
import Link from "next/link";
const PersonNameWrapper = styled.p.attrs((props) => ({
  className: props.className,
}))`
  color: ${(props) => props.color || "#0082fc"};
  font-size: ${(props) => props.fontSize || "16px"};
  cursor: pointer;
  margin-bottom: ${(props) => props.marginBottom || "0px"}; ;
`;
interface Props {
  uid: number;
  name: string;
  className?: string;
  style?: React.CSSProperties;
}
export default function ToPersonPage(props: Props) {
  const { name, style, className, uid } = props;
  return (
    <Link href={`/user/${uid}`}>
      <a target="_blank" rel="noreferrer">
        <PersonNameWrapper {...style} className={className}>
          {name}
        </PersonNameWrapper>
      </a>
    </Link>
  );
}
