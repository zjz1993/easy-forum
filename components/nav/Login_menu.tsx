import React from "react";
import { UserInfoTypes } from "../../types/login_type";
import styles from "./styles.module.less";
import { Action, Dispatch } from "redux";
import { logout } from "../../actions/loginActions";
import Link from "next/link";

type Props = {
  loginInfo: UserInfoTypes;
  dispatch: Dispatch<Action>;
};
const LoginMenu: React.FC<Props> = ({ loginInfo, dispatch }) => {
  return (
    <div className={styles.login_menu_wrapper}>
      <div className={styles.item}>
        {loginInfo.isAdmin
          ? `欢迎管理员:${loginInfo.username}`
          : `欢迎您${loginInfo.username}`}
      </div>
      <div className={styles.item}>
        <Link href={`/user/${loginInfo.uid}`}>个人中心</Link>
      </div>
      <div
        className={styles.item}
        onClick={() => {
          dispatch(logout());
        }}
      >
        退出登录
      </div>
    </div>
  );
};
export default LoginMenu;
