import React from "react";
import { Popover } from "antd";
import { UserInfoTypes } from "../../types/login_type";
import LoginMenu from "./Login_menu";
import { connect } from "react-redux";
import { Action, Dispatch } from "redux";
type Props = {
  loginInfo: UserInfoTypes;
  dispatch: Dispatch<Action>;
};
const LoginedNav: React.FC<Props> = ({ loginInfo, dispatch }) => {
  return (
    <Popover content={LoginMenu({ loginInfo, dispatch })}>
      <div>
        <img
          style={{ borderRadius: 16 }}
          src={loginInfo.avatarUrl}
          width={32}
          height={32}
        />
      </div>
    </Popover>
  );
};

export default connect()(LoginedNav);
