import React, { useRef, useState } from "react";
import styles from "./styles.module.less";
import classNames from "classnames";
import { Button } from "antd";
import { useMount } from "ahooks";
import { UserInfoTypes } from "../../types/login_type";
type Props = {
  placeholder?: string;
  onBlur?: () => void;
  autoFocus?: boolean;
  loginInfo?: UserInfoTypes;
};
const CommentEditor: React.FC<Props> = ({
  onBlur = () => {},
  placeholder = "请输入评论",
  autoFocus = true,
  loginInfo,
}) => {
  const editorRef = useRef(null);
  const [html, setHtml] = useState("");
  const [isFocus, setIsFocus] = useState(false);
  useMount(() => {
    if (autoFocus) {
      editorRef.current && editorRef.current.focus();
    }
  });
  return (
    <>
      <div className={styles.editor_wrapper}>
        <div
          ref={editorRef}
          onFocus={() => {
            console.log("focus");
            setIsFocus(true);
          }}
          onBlur={() => {
            onBlur && onBlur();
            setIsFocus(false);
          }}
          onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
            setHtml(e.target.innerHTML);
          }}
          contentEditable
          className={classNames(styles.editor, {
            [styles.empty]: html.length === 0,
          })}
          placeholder={loginInfo ? placeholder : "请登录后输入评论"}
        />
      </div>
      {isFocus && (
        <div className={styles.footer}>
          <Button className={styles.left_btn}>取消</Button>
          <Button type="primary" disabled={html.length === 0}>
            发表
          </Button>
        </div>
      )}
    </>
  );
};
export default CommentEditor;
