import React from "react";
import Head from "next/head";

interface Props {
  title?: string;
}

export default function Header(props: Props) {
  const { title = "easy forum" } = props;
  return (
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link
        rel="stylesheet"
        type="text/css"
        href="https://at.alicdn.com/t/font_2430064_ah8kezt20ek.css"
      />
    </Head>
  );
}
