import React from "react";
interface Props {
  html: string;
}
const Html: React.FC<Props> = (props) => {
  const { html } = props;
  return <div dangerouslySetInnerHTML={{ __html: html }} />;
};
export default Html;
