import React, { useEffect } from "react";
import styles from "./styles.module.less";
import ImageZoom from "../imageZoom";
import Image from "next/image";
import Html from "./html";
import MyModal from "../imageZoom/MyModal";
interface Props {
  html: string;
}
const CustomHtml: React.FC<Props> = (props) => {
  const { html } = props;
  const wrapperRef = React.useRef<HTMLInputElement>(null);
  const handleClick = (e) => {
    const clickDom = e.target as HTMLInputElement;
    if (clickDom.tagName === "IMG") {
      console.log("点击图片了");
      const src = clickDom.getAttribute("src");
      ImageZoom.zoom();
      console.log(src);
    }
  };
  useEffect(() => {
    wrapperRef.current.addEventListener("click", (e) => handleClick(e));
    return wrapperRef.current.removeEventListener("click", handleClick);
  }, []);
  return (
    <div>
      <MyModal />
      <div
        ref={wrapperRef}
        className={styles.article}
        dangerouslySetInnerHTML={{ __html: html }}
      />
    </div>
  );
};
export default CustomHtml;
