import React, { useEffect, useRef, useState } from "react";
import Vditor from "vditor";
import "vditor/dist/index.css";
import { Spin } from "antd";
const PageEditor: React.FC = () => {
  const editorRef = useRef();
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    console.log(editorRef);
    const vditor = new Vditor(editorRef.current, {
      height: 360,
      toolbarConfig: {
        pin: true,
      },
      cache: {
        enable: false,
      },
      after() {
        vditor.setValue("Hello, Vditor + React!");
        setLoading(false);
      },
    });
  }, []);
  return (
    <div>
      {loading && (
        <div>
          <Spin />
        </div>
      )}
      <div ref={editorRef} />
    </div>
  );
};
export default PageEditor;
