import React from "react";
import styled from "styled-components";
const FooterWrapper = styled.div`
  position: sticky;
  bottom: 0;
  height: 50px;
  background-color: black;
  width: 100%;
`;
export default function Footer() {
  return <FooterWrapper>尾部</FooterWrapper>;
}
