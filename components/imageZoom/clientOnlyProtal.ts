import { useEffect, useState } from "react";
import { createPortal } from "react-dom";

export default function ClientOnlyPortal({ children, selector }) {
  // const ref = useRef();
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    // ref.current = document.querySelector(selector);
    console.log(selector);
    setMounted(true);
  }, [selector]);

  return mounted ? createPortal(document.body, selector.current) : null;
}
