import React from "react";
import ImageZoomContainer from "./container";
import { Modal } from "antd";
export default class ImageZoom {
  static zoom() {
    Modal.info({
      content: <ImageZoomContainer />,
    });
  }
}
