import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "../reducers";
import { createWrapper, Context } from "next-redux-wrapper";

export const initStore = (context: Context) => {
  return createStore(reducer, applyMiddleware(thunk));
};
export const wrapperStore = createWrapper(initStore, { debug: true });
