import React from "react";
import Header from "../components/Header";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
// import dynamic from "next/dynamic";
// const DynamicComponent = dynamic(() => import("../components/Footer"), {
//   loading: () => <h1>加载中</h1>,
// });

interface Props {
  children: React.ReactNodeArray;
}
function BasicLayout(props: Props) {
  const { children } = props;
  return (
    <>
      <Header />
      <Nav />
      <div className="app_container">{children}</div>
      <Footer />
    </>
  );
}
export default BasicLayout;
