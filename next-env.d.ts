/// <reference types="next" />
/// <reference types="next-react-svg" />
/// <reference types="next/types/global" />
/// <reference types="next-images" />
declare module "*.less" {
  const resource: { [key: string]: string };
  export = resource;
}
