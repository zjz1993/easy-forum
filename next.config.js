const withPlugins = require("next-compose-plugins");
const withReactSvg = require("next-react-svg");
const withImages = require("next-images");
const withAntdLess = require("next-plugin-antd-less");
const { i18n } = require("./next-i18next.config");

const path = require("path");
const nextConfig = {
  distDir: "build",
  images: {
    domains: [""],
  },
  webpack: (config, options) => {
    return config;
  },
  i18n,
};
module.exports = withPlugins(
  [
    [withAntdLess],
    [withImages],
    [
      withReactSvg,
      {
        include: path.resolve(__dirname, "public"),
      },
    ],
  ],
  nextConfig
);
