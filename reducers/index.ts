import { combineReducers } from "redux";
import authReducer from "./authReducer";
import userReducer from "./loginReducer";
const rootReducer = combineReducers<{ authentication: any; userInfo: any }>({
  authentication: authReducer,
  userInfo: userReducer,
});
export default rootReducer;
