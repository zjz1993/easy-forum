import * as ActionTypes from "../../actions/types";
import { HYDRATE } from "next-redux-wrapper";
import { UserInfoTypes } from "../../types/login_type";

export interface State {
  loginInfo?: UserInfoTypes;
}
const initState = { loginInfo: undefined };

const reducer = (
  state: State = initState,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case ActionTypes.LOGIN_BY_ACCOUNT:
      return { ...state, loginInfo: action.payload };
    case ActionTypes.LOGOUT:
      return { ...state, loginInfo: undefined };
    case HYDRATE:
      if (action.payload.app === "init") delete action.payload.app;
      if (action.payload.page === "init") delete action.payload.page;
      return { ...state, ...action.payload };
    case "APP":
      return { ...state, app: action.payload };
    case "PAGE":
      return { ...state, page: action.payload };
    default:
      return state;
  }
};
export default reducer;
