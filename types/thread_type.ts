export type ThreadIndexInfoTypes = {
  id: number;
  author: string;
  authorId: number;
  title: string;
  createtime: number;
  collect_num: number;
  comment_num: number;
  agree_num: number;
  isAgree: boolean;
};
export type ThreadDetailInfoTypes = {
  title: string;
  createtime: number;
  message: string;
  isAuthor: boolean;
  isLike: boolean;
  like_num: number;
  isCollect: boolean;
  collect_num: number;
  author: string;
  authorid: number;
};
export type CommentInfoTypes = {
  id: number;
  isAuthor: boolean;
  author: string;
  isLike: boolean;
  like_num: number;
  authorid: number;
  createtime: number;
  message: string;
  replyCount: number;
  reply: Array<ReplyInfoTypes>;
};
export type ReplyInfoTypes = {
  id: number;
  isAuthor: boolean;
  author: string;
  authorid: number;
  replyed_author: string;
  replyed_authorid: number;
  createtime: number;
  message: string;
  isLike: boolean;
  like_num: number;
};
