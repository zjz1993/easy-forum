export type UserInfoTypes = {
  uid: number;
  username: string;
  isAdmin: boolean;
  avatarUrl: string;
};
