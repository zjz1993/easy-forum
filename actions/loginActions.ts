import * as ActionTypes from "./types";
import { UserInfoTypes } from "../types/login_type";
export function loginByUsername(params: UserInfoTypes) {
  return {
    type: ActionTypes.LOGIN_BY_ACCOUNT,
    payload: params,
  };
}
export function logout() {
  return {
    type: ActionTypes.LOGIN_BY_ACCOUNT,
  };
}
