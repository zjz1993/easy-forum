import React from "react";
import { useRouter } from "next/router";

const Post: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  console.log(id);
  return <p>个人主页Post: {id}</p>;
};

export default Post;
