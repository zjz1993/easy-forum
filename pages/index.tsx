import React from "react";
import IndexHeader from "../components/pageComponent/index/indexHeader";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { connect } from "react-redux";
import { UserInfoTypes } from "../types/login_type";
import styles from "./indexPage.module.less";
import IndexCategory from "../components/pageComponent/index/indexCategory";
import IndexListItem from "../components/pageComponent/index/indexList";
import { Empty } from "antd";

type Props = {
  loginInfo?: UserInfoTypes;
};
const thread_list = [
  {
    id: 1,
    author: "zhaojunzhe",
    authorId: 2,
    title: "与君（FR）初相识,犹如故人归，浅谈FCRA认证",
    createtime: 1627541443,
    collect_num: 1,
    comment_num: 2,
    agree_num: 12,
    isAgree: false,
  },
];
const Home: React.FC<Props> = ({ loginInfo }) => {
  return (
    <div className={styles.home_wrapper}>
      <div className={styles.content_wrapper}>
        <div className={styles.content_left}>
          <IndexHeader loginInfo={loginInfo} />
          <div>
            {thread_list.length === 0 ? (
              <div className={styles.empty_wrapper}>
                <Empty description={"暂无帖子"} />
              </div>
            ) : (
              thread_list.map((item) => {
                return (
                  <IndexListItem
                    item={item}
                    key={item.id}
                    loginInfo={loginInfo}
                  />
                );
              })
            )}
          </div>
        </div>
        <IndexCategory fid={0} />
      </div>
    </div>
  );
};
// Home.layout = "admin";
const mapStateToProps = (state) => {
  return {
    loginInfo: state.userInfo.loginInfo,
  };
};
export default connect(mapStateToProps)(Home);
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common"])),
  },
});
