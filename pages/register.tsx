import React from "react";
import { Form, Input, Button, message } from "antd";
import Link from "next/link";
import styles from "../components/pageComponent/login/login.module.less";
import { router } from "next/client";
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Register: React.FC = () => {
  const onFinish = (values: any) => {
    console.log("Success:", values);
    if (values.password !== values.password_confirm) {
      message.warning("两次密码不一致，请重新输入");
    } else {
      // TODO 注册逻辑
      message.success("成功注册,请登录", 1, () => {
        router.push({
          pathname: "/login",
        });
      });
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className={styles.loginWrapper}>
      <div className={styles.loginWrapper_inner}>
        <h1 className={styles.title}>注册</h1>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: "请输入用户名" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: "请输入密码" }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="重复密码"
            name="password_confirm"
            rules={[{ required: true, message: "请输入密码" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              className={styles.login_btn}
            >
              注册
            </Button>
            <p>
              已有账号,点此<Link href={"/login"}>登录</Link>
            </p>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default Register;
