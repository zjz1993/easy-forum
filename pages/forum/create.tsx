import React from "react";
import { Button, Dropdown, Menu, message, Input } from "antd";
import "vditor/dist/index.css";
import styles from "./styles.module.less";
import { useRouter } from "next/router";
import PageEditor from "../../components/pageEditor";

const menu = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.antgroup.com"
      >
        1st menu item
      </a>
    </Menu.Item>
    <Menu.Item danger>a danger item</Menu.Item>
  </Menu>
);

export default function ThreadCreate() {
  const router = useRouter();
  const submit = () => {
    const id = 1;
    message.success("发布成功", 1, () => {
      router.push({
        pathname: "/thread/[id]",
        query: { id },
      });
    });
  };
  return (
    <div className={styles.create_wrapper}>
      <div className={styles.title}>发表新帖</div>
      <div className={styles.formitem}>
        <div className={styles.label}>输入帖子标题</div>
        <Input className={styles.content} />
      </div>
      <div className={styles.operation_wrapper}>
        <div className={styles.title}>选择板块</div>
        <Dropdown overlay={menu} trigger={["click"]}>
          <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
            默认板块
          </a>
        </Dropdown>
      </div>
      <PageEditor />
      <div className={styles.footer}>
        <Button
          type="primary"
          className={styles.footer_left_btn}
          onClick={submit}
        >
          发布
        </Button>
        <Button>取消</Button>
      </div>
    </div>
  );
}
