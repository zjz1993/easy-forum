import React, { useEffect, useRef } from "react";
import { Button, message } from "antd";
import Vditor from "vditor";
import "vditor/dist/index.css";
import styles from "../styles.module.less";
import { useRouter } from "next/router";

export default function EditThread() {
  const editorRef = useRef();
  const router = useRouter();
  useEffect(() => {
    const vditor = new Vditor(editorRef.current, {
      height: 360,
      toolbarConfig: {
        pin: true,
      },
      cache: {
        enable: false,
      },
      after() {
        vditor.setValue("Hello, Vditor + React!");
      },
    });
  }, []);
  const submit = () => {
    const id = 1;
    message.success("保存成功", 1, () => {
      router.push({
        pathname: "/thread/[id]",
        query: { id },
      });
    });
  };
  return (
    <div className={styles.create_wrapper}>
      <div className={styles.title}>编辑帖子</div>
      <div className={styles.operation_wrapper}>
        <div className={styles.title}>选择板块</div>
      </div>
      <div ref={editorRef} />
      <div className={styles.footer}>
        <Button
          type="primary"
          className={styles.footer_left_btn}
          onClick={submit}
        >
          保存
        </Button>
        <Button>取消</Button>
      </div>
    </div>
  );
}
