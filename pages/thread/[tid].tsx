import React, { useContext, useRef, useState } from "react";
import { useRouter } from "next/router";
import styles from "./styles.module.less";
import { Dropdown, Menu } from "antd";
import Link from "next/link";
import ThemeContext from "../../context";
import Comment from "../../components/pageComponent/thread/comment";
import CommentEditor from "../../components/commentEditor";
import CustomHtml from "../../components/customHtml";
import { connect } from "react-redux";
import { UserInfoTypes } from "../../types/login_type";
import Tool from "../../components/pageComponent/thread/tool";
import {
  CommentInfoTypes,
  ThreadDetailInfoTypes,
} from "../../types/thread_type";

type Props = {
  loginInfo?: UserInfoTypes;
};

const ThreadPage: React.FC<Props> = ({ loginInfo }) => {
  const [threadData] = useState<ThreadDetailInfoTypes>({
    title: "社区人才库意见征集",
    createtime: 1618830677318,
    message:
      '<p>在填报报表里，数据库的主键与帆软设置的主键是一致的</p><p><span .="color: rgb(51, 51, 51); font-family: PingFangSC-Light, &amp;quot;Hiragino Sans GB&amp;quot;, &amp;quot;Microsoft YaHei&amp;quot;, OpenSans, Verdana; font-size: 14px; background-color: rgb(247, 249, 253);">数据录完后，如果录入数据的主键与数据库里已有的主键重复的话会替换</span><img src="https://bbs.fanruan.com/upload/wenda/20210420/1618907408812299.png" title="1618907408812299.png" alt="微信截图_20210420162921.png"/><img src="https://bbs.fanruan.com/upload/wenda/20210420/1618907408812299.png" title="1618907408812299.png" alt="微信截图_20210420162921.png"/></p>',
    isAuthor: true,
    isLike: false,
    like_num: 20,
    isCollect: false,
    collect_num: 20,
    author: "zhaojunzhe",
    authorid: 230080,
  });
  const [commentData] = useState<Array<CommentInfoTypes>>([
    {
      id: 1,
      isAuthor: false,
      author: "评论作者",
      authorid: 1,
      createtime: 1618830677318,
      message: "<p>评论</p>",
      replyCount: 7,
      isLike: true,
      like_num: 20,
      reply: [
        {
          isLike: true,
          like_num: 20,
          id: 2,
          isAuthor: false,
          author: "回复者",
          authorid: 2,
          replyed_author: "被回复者",
          replyed_authorid: 3,
          createtime: 1618830677318,
          message: "<p>回复1</p>",
        },
        {
          id: 3,
          isAuthor: false,
          author: "回复者",
          authorid: 2,
          replyed_author: "被回复者",
          replyed_authorid: 3,
          createtime: 1618830677318,
          message: "<p>回复2</p>",
          isLike: true,
          like_num: 20,
        },
      ],
    },
  ]);
  const value = useContext(ThemeContext);
  const router = useRouter();
  const test = useRef({ isInBottom: false });
  test.current = value;
  const { tid } = router.query;
  const menu = (
    <Menu>
      <Menu.Item>
        <Link href={`/forum/edit/${tid}`}>编辑</Link>
      </Menu.Item>
      <Menu.Item danger>删除</Menu.Item>
    </Menu>
  );
  React.useEffect(() => {
    return () => {
      console.log("value变化");
      console.log(test.current);
    };
  }, [value]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.wrapper_left}>
        <div className={styles.title_wrapper}>
          <div className={styles.title}>{threadData.title}</div>
          {loginInfo &&
          (loginInfo.uid === threadData.authorid || loginInfo.isAdmin) ? (
            <Dropdown overlay={menu} trigger={["click"]}>
              <span
                onClick={(e) => e.preventDefault()}
                className={styles.manage}
              >
                管理
              </span>
            </Dropdown>
          ) : null}
        </div>
        <div className={styles.head_info}>
          <div className={styles.tag_wrapper}>
            <a className={styles.tag}>帆软动态</a>
            <a className={styles.tag}>帆软动态2</a>
          </div>
          <div className={styles.line} />
          <div className={styles.timer}>发布于3分钟前</div>
        </div>
        <div className={styles.wrapper_left_body}>
          <CustomHtml html={threadData.message} />
        </div>
        <Tool
          loginInfo={loginInfo}
          isLike={threadData.isLike}
          like_num={threadData.like_num}
          isCollect={threadData.isCollect}
          collect_num={threadData.collect_num}
        />
        <div>
          <CommentEditor autoFocus={false} loginInfo={loginInfo} />
        </div>
        <div className={styles.comment_wrapper}>
          {commentData.map((comment) => (
            <Comment comment={comment} key={comment.id} loginInfo={loginInfo} />
          ))}
        </div>
      </div>
      <div className={styles.wrapper_right}>123</div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    loginInfo: state.userInfo.loginInfo,
  };
};
export default connect(mapStateToProps)(ThreadPage);
