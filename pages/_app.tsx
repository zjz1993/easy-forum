import React from "react";
import App from "next/app";
import { compose } from "redux";
import { appWithTranslation } from "next-i18next";
import { wrapperStore } from "../store";
import "antd/dist/antd.css";
import "../global.less";
import LayoutWrapper from "../layout/index";
import ThemeContext from "../context";
import Util from "../util/index";
import { BackTop } from "antd";
import nextI18NextConfig from "../next-i18next.config.js";

class MyApp extends App<any> {
  state = {
    isInBottom: false,
  };
  static async getInitialProps({ Component, ctx }) {
    return {
      pageProps: {
        // Call page-level getInitialProps
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
      },
    };
  }
  monitorScroll = () => {
    if (Util.isInBottom()) {
      console.log("到底部");
      this.setState({ isInBottom: true });
    } else {
      if (this.state.isInBottom) {
        this.setState({ isInBottom: false });
      }
    }
  };
  componentDidMount() {
    console.log("加载_app");
    document.addEventListener("scroll", this.monitorScroll);
  }
  componentWillUnmount() {
    document.removeEventListener("scroll", this.monitorScroll);
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <LayoutWrapper {...pageProps}>
        <ThemeContext.Provider value={{ isInBottom: this.state.isInBottom }}>
          <Component {...pageProps} {...this.props} />
          <BackTop target={() => document} visibilityHeight={500} style={{}} />
        </ThemeContext.Provider>
      </LayoutWrapper>
    );
  }
}
export default compose(appWithTranslation, wrapperStore.withRedux)(MyApp);
