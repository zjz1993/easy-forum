export const csspoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};
export const mediaQueries = (key: any) => {
  return (style: any) => `@media (min-width: ${csspoints[key]}em) { ${style} }`;
};
